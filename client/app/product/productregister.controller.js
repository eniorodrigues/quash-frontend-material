(function () {
    'use strict';

    angular.module('app.product')
        .controller('ProductregisterCtrl', ['$scope', '$filter', '$log', 'productFactory', '$rootScope', '$q', '$http', '$mdDialog', 'productService', ProductregisterCtrl]);

    function ProductregisterCtrl($scope, $filter, $log, productFactory, $rootScope, $q, $http, $mdDialog, productService) {
        
        $scope.productVO = {};
        $scope.someService = productService;
        $scope.productForm = [];
        $scope.status = '  ';
        $scope.url;
        $scope.status;
        $rootScope.url;
 
        var message;
        $scope.showAlert = function(ev) {
            $mdDialog.show(
                $mdDialog.alert(message)
                    .parent(angular.element(document.querySelector('#popupContainer')))
                    .clickOutsideToClose(true)
                    .title('Info')
                    .content(message)
                    .ariaLabel('Info')
                    .ok('OK')
                    .targetEvent(ev)
            );
        };
        
        //findProductByIdProductview achar o produto de acordo com o id 
            if(productService.id != undefined){
                  productFactory.findProductByIdProductview(productService.id).success(function(data){
                  $scope.productVO =  data;
                  });
              } else{
                    $scope.productVO ={};                                                           
            }

        $scope.submit = function(){
        $scope.productVO.companyVO = {};
        $scope.productVO.companyVO.cocode = "0000";
            
            if($scope.productVO.idproduct === undefined){
            
                productFactory.submitProductregister($scope.productVO).success(function(data){
                                       
                        console.log(data.messageTypeEnumVO);
                     if(data.messageTypeEnumVO === "ERROR"){
                                message = data.messageError;
                                $scope.showAlert(message);
                        } else{
                             message = "Product registered";
                            $scope.showAlert(message);  
                               
                        }

                    });
                
                
                } else{
                    productFactory.editProductregister($scope.productVO).success(function(data){
                        $scope.showAlert(message);
                    });
                }
        };
        

    }

})();