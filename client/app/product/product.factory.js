var module = angular.module('app.product');

module.factory('productFactory',['$http', '$rootScope', function ($http, $rootScope) {
            
            var productFactory = {};

        productFactory.getProductview = function(pageSize, first){
                $rootScope.url;
                var path =  "/quash-backend-ws/rest/v0/product/findProductBySearch?cocode=0000&first=" + first + "&pageSize=" +  pageSize;
                var url = $rootScope.url + path;
                    return $http.get(url);
        };
        
        productFactory.findProductByIdProductview = function(idproduct){
              $rootScope.url;
              var path = '/quash-backend-ws/rest/v0/product/findProductById/' + idproduct
              var url = $rootScope.url + path;
                    return $http.get(url);
        };

        productFactory.editProductview = function(){
            $rootScope.url;
              var path = '/quash-backend-ws/rest/v0/product/edit'
              var url = $rootScope.url + path;
                    return $http.post(url);
        };

       productFactory.deleteProductview = function(idproduct){
              $rootScope.url;
              var path = '/quash-backend-ws/rest/v0/product/delete/'+ idproduct 
              var url = $rootScope.url + path;
                    return $http.get(url);
       };
    
       productFactory.submitProductregister = function(productVO){
             $rootScope.url;

             var path =  '/quash-backend-ws/rest/v0/product/save'
             var url = $rootScope.url + path; 
                return $http.post(url, productVO);
       };
    
       productFactory.editProductregister = function(productVO){
         $rootScope.url;

         var path =  '/quash-backend-ws/rest/v0/product/edit'
         var url = $rootScope.url + path; 
            return $http.post(url, productVO);
       };

         return productFactory;

        }]);

        module.factory('httpRequestInterceptor', function () {
          return {
            request: function (config) {
              config.headers['Auth-token'] = '0000:rodrigo.klein:abcd1234';
              //config.headers['Accept'] = 'application/json;odata=verbose';
              return config;
            }
          };
        });

        module.config(function ($httpProvider) {
          $httpProvider.interceptors.push('httpRequestInterceptor');
        });