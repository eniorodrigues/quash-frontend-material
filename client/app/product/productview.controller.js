(function () {
    'use strict';

    angular.module('app.product')
        .controller('ProductviewCtrl', ['$scope', '$filter', '$log', 'productFactory', '$rootScope', '$q', '$http', '$mdDialog', 'productService' ,  ProductviewCtrl]);

    function ProductviewCtrl($scope, $filter, $log, productFactory, $rootScope, $q, $http, $mdDialog, productService) {

        var init;
        $scope.productVO = {};
        $scope.url;
        $scope.status;
        $rootScope.url;
        $scope.productForm = [];
        $scope.numPerPageOpt = [3, 5, 10, 20];
        $scope.numPerPage = $scope.numPerPageOpt[1];
        $scope.onFilterChange = onFilterChange;
        $scope.onNumPerPageChange = onNumPerPageChange;
        $scope.currentPage = 1;
        $scope.currentPage = [];
        $scope.select = select;
        $scope.previous = previous;
        $scope.filteredStores = [];
        $scope.next = next;
        var pageSize;
        var first = 0;
        var endoflist = false;

        //Mostra tela de alert para producto deleteado
        $scope.showAlert = function(ev) {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.querySelector('#popupContainer')))
                    .clickOutsideToClose(true)
                    .title('Success')
                    .content('Product deleted successfully')
                    .ariaLabel('Sucess!')
                    .ok('OK')
                    .targetEvent(ev)
            );
        };
        
        //Mostra tela de confirmação para deletar produto
        $scope.showConfirm = function(idproduct) {
            var confirm = $mdDialog.confirm()
                        .title('Would you like to delete the product?')
                        .content('Delete')
                        .ariaLabel('Delete')
                        .targetEvent(idproduct)
                        .cancel('NO')
                        .ok('YES');
            $mdDialog.show(confirm).then(function(event) {
                $scope.delete(idproduct);
            }, function() {
               
            });
        };

        // Function Load Product carrega produto
        function loadProductFactory(){
        var json;
            productFactory.getProductview(pageSize, first).success(function(data){
                 json = data;
                    if(json.length>0){
                            $scope.productVO = json;
                    }else{
                        // Controle para quando a paginação chega no final da lista....
                        endoflist = true;
                        first -= pageSize;
                    }
            })
            .error(function(error){
               $scope.status = 'Unable to load products' + error.message;
            });
        }
        // Fim Function Load Product carrega produto
        
        // FindProduct codigo do produto
        $scope.findProduct = function(idproduct){
                     productService.setProperty(idproduct);
        }
        
        //Fim findProduct achar produto por codigo
        
        // showProduct realiza a tradução do status 
         $scope.showProducts = function(status) {
           return status == 10 ? 'Active' : 'Deleted';
         };
        //fim showProduct realiza a tradução do status 
        
        // DELETA produto
        $scope.delete = function(idproduct){
            productFactory.deleteProductview(idproduct).success(function(data){
                loadProductFactory();
                $scope.showAlert ();
            })
            .error(function(erro){
                console.log(erro);
                $scope.message = 'Unable to delete product' + error.message;
            })
        };
        // FIM DELETA produto
        
         // metodo inicial init 
        init = function() {
            $scope.select(0);
        };
        init();
        // FIM init
        
        //Função NEXT da paginação
        function next(){
            first += pageSize;
            loadProductFactory();
        };
        //FIM Função NEXT da paginação
        
        //Função PREVIOUS da paginação
        function previous(){
            var sum = first - pageSize;
            if(sum >= 0){
                first = first - pageSize ;
                 loadProductFactory();
                }else{
                first = 0;
                    }
        }
        //FIM Função PREVIOUS da paginação

        //metodo de selecionar pelo combobox
        function select(page) {
            var end, start;
            start = (page - 1) ;
            end = $scope.numPerPage;
            pageSize = end;
            loadProductFactory();
        };
         //fim metodo de selecionar pelo combobox

        //onFilterChange selecionar a pelo mudança do combobox
        function onFilterChange() {
            $scope.select(1);
            $scope.currentPage = 1;
            return $scope.row = '';
        };

        //onNumPerPageChange selecionar a pelo mudança do combobox
        function onNumPerPageChange() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };
       
    }

})();