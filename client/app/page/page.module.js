(function () {
    'use strict';

    angular.module('app.page', []);
    angular.module('Authentication', []);
    angular.module('Home', []);
})(); 