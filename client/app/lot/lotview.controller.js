(function () {
    'use strict';

    angular.module('app.lot')
        .controller('LotviewCtrl', ['$scope', '$filter', '$log', 'lotFactory', '$rootScope', '$q', '$http', '$mdDialog', 'lotService' ,  LotviewCtrl]);

    function LotviewCtrl($scope, $filter, $log, lotFactory, $rootScope, $q, $http, $mdDialog, lotService) {

        var init;
        $scope.lotVO = {};
        $scope.url;
        $scope.status;
        $rootScope.url;
        $scope.lotForm = [];
        $scope.numPerPageOpt = [3, 5, 10, 20];
        $scope.numPerPage = $scope.numPerPageOpt[1];
        $scope.onFilterChange = onFilterChange;
        $scope.onNumPerPageChange = onNumPerPageChange;
        $scope.currentPage = 1;
        $scope.currentPage = [];
        $scope.select = select;
        $scope.previous = previous;
        $scope.filteredStores = [];
        $scope.next = next;
        var pageSize;
        var first = 0;
        var endoflist = false;

        //Mostra tela de alert para loto deleteado
        $scope.showAlert = function(ev) {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.querySelector('#popupContainer')))
                    .clickOutsideToClose(true)
                    .title('Success')
                    .content('Lot deleted successfully')
                    .ariaLabel('Sucess!')
                    .ok('OK')
                    .targetEvent(ev)
            );
        };
        
        //Mostra tela de confirmação para deletar produto
        $scope.showConfirm = function(idlot) {
            var confirm = $mdDialog.confirm()
                        .title('Would you like to delete the lot?')
                        .content('Delete')
                        .ariaLabel('Delete')
                        .targetEvent(idlot)
                        .cancel('NO')
                        .ok('YES');
            $mdDialog.show(confirm).then(function(event) {
                $scope.delete(idlot);
            }, function() {
               
            });
        };

        // Function Load Lot carrega produto
        function loadLotFactory(){
        var json;
            lotFactory.getLotview(pageSize, first).success(function(data){
                 json = data;
                    if(json.length>0){
                            $scope.lotVO = json;
                    }else{
                        // Controle para quando a paginação chega no final da lista....
                        endoflist = true;
                        first -= pageSize;
                    }
            })
            .error(function(error){
               $scope.status = 'Unable to load lots' + error.message;
            });
        }
        // Fim Function Load Lot carrega produto
        
        // FindLot codigo do produto
        $scope.findLot = function(idlot){
                     lotService.setProperty(idlot);
        }
        
        //Fim findLot achar produto por codigo
        
        // showLot realiza a tradução do status 
         $scope.showLots = function(status) {
           return status == 10 ? 'Active' : 'Deleted';
         };
        //fim showLot realiza a tradução do status 
        
        // DELETA produto
        $scope.delete = function(idlot){
            lotFactory.deleteLotview(idlot).success(function(data){
                loadLotFactory();
                $scope.showAlert ();
            })
            .error(function(erro){
                console.log(erro);
                $scope.message = 'Unable to delete lot' + error.message;
            })
        };
        // FIM DELETA produto
        
         // metodo inicial init 
        init = function() {
            $scope.select(0);
        };
        init();
        // FIM init
        
        //Função NEXT da paginação
        function next(){
            first += pageSize;
            loadLotFactory();
        };
        //FIM Função NEXT da paginação
        
        //Função PREVIOUS da paginação
        function previous(){
            var sum = first - pageSize;
            if(sum >= 0){
                first = first - pageSize ;
                 loadLotFactory();
                }else{
                first = 0;
                    }
        }
        //FIM Função PREVIOUS da paginação

        //metodo de selecionar pelo combobox
        function select(page) {
            var end, start;
            start = (page - 1) ;
            end = $scope.numPerPage;
            pageSize = end;
            loadLotFactory();
        };
         //fim metodo de selecionar pelo combobox

        //onFilterChange selecionar a pelo mudança do combobox
        function onFilterChange() {
            $scope.select(1);
            $scope.currentPage = 1;
            return $scope.row = '';
        };

        //onNumPerPageChange selecionar a pelo mudança do combobox
        function onNumPerPageChange() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };
       
    }

})();