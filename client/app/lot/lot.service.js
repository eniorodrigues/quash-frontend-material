var module = angular.module('app.lot');

    module.factory('lotService', function () {
        
       var service = {};
        
        service.id;
        
         service.setProperty = function(_id){
             
            service.id = _id;
             
            return service;
        }

        return service;
    });
