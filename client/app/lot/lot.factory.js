var module = angular.module('app.lot');

module.factory('lotFactory',['$http', '$rootScope', function ($http, $rootScope) {
            
            var lotFactory = {};

        lotFactory.getLotview = function(pageSize, first){
                $rootScope.url;
                var path =  "/quash-backend-ws/rest/v0/lot/findLotBySearch?cocode=0000&first=" + first + "&pageSize=" +  pageSize;
                var url = $rootScope.url + path;
                    return $http.get(url);
        };
        
        lotFactory.findLotByIdLotview = function(idlot){
              $rootScope.url;
              var path = '/quash-backend-ws/rest/v0/lot/findLotById/' + idlot
              var url = $rootScope.url + path;
                    return $http.get(url);
        };

        lotFactory.editLotview = function(){
            $rootScope.url;
              var path = '/quash-backend-ws/rest/v0/lot/edit'
              var url = $rootScope.url + path;
                    return $http.post(url);
        };

       lotFactory.deleteLotview = function(idlot){
              $rootScope.url;
              var path = '/quash-backend-ws/rest/v0/lot/delete/'+ idlot 
              var url = $rootScope.url + path;
                    return $http.get(url);
       };
    
       lotFactory.submitLotregister = function(lotVO){
             $rootScope.url;

             var path =  '/quash-backend-ws/rest/v0/lot/save'
             var url = $rootScope.url + path; 
                return $http.post(url, lotVO);
       };
    
       lotFactory.editLotregister = function(lotVO){
         $rootScope.url;

         var path =  '/quash-backend-ws/rest/v0/lot/edit'
         var url = $rootScope.url + path; 
            return $http.post(url, lotVO);
       };

         return lotFactory;

        }]);

        module.factory('httpRequestInterceptor', function () {
          return {
            request: function (config) {
              config.headers['Auth-token'] = '0000:rodrigo.klein:abcd1234';
              //config.headers['Accept'] = 'application/json;odata=verbose';
              return config;
            }
          };
        });

        module.config(function ($httpProvider) {
          $httpProvider.interceptors.push('httpRequestInterceptor');
        });