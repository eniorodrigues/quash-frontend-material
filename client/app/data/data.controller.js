(function () {
    'use strict';

    angular.module('app.data')
        .controller('DataCtrl', ['$scope', '$filter','$http', '$log', 'dataFactory', '$rootScope', DataCtrl]);


    function DataCtrl($scope, $filter, $http, $log, dataFactory, $rootScope) {
        
        $scope.products ;
        $scope.url;
        $scope.status;
        $rootScope.url;
        
        $scope.coisa =   $rootScope.url;

        function loadDataFactory(){
            dataFactory.getData().success(function(data){
                 $scope.products = data; 
        })
            .error(function(error){
               $scope.status = 'Unable to load products' +    error.message;
          });
       }
        
        loadDataFactory();
    
    }

})(); 