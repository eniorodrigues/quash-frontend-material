(function () {
    'use strict';

    angular.module('app.userview')
        .controller('UserviewCtrl', ['$scope', '$filter', '$log', 'userFactory', '$rootScope', UserviewCtrl]);

    function UserviewCtrl($scope, $filter, $log, userFactory, $rootScope) {
  
        $scope.ocw;
        
        $scope.url;
        $scope.status;
        $rootScope.url;

        function loadUserFactory(){
            userFactory.getUserview().success(function(data){
                 var json =  data;
                $scope.ocw = json;
        })
            .error(function(error){
               $scope.status = 'Unable to load users' + error.message;
          });
       }
        
        loadUserFactory();
    
    }

})();