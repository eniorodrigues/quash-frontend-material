var module = angular.module('app.userview');

module.factory('userFactory',['$http', '$rootScope', function ($http, $rootScope) {
            
            var userFactory = {};
    
            userFactory.getUserview = function(){
                $rootScope.url;
                var path =  "/quash-backend-api/rest/user/getAllUserWS"
                var url = $rootScope.url + path;
                    return $http.get(url);
        }
            
     return userFactory;
    
	}]);

        module.factory('httpRequestInterceptor', function () {
          return {
            request: function (config) {

              config.headers['Auth-token'] = '0000:rodrigo.klein:abcd1234';
              //config.headers['Accept'] = 'application/json;odata=verbose';

              return config;
            }
          };
        });

        module.config(function ($httpProvider) {
          $httpProvider.interceptors.push('httpRequestInterceptor');
        });